/* 
 * The intent of this file is to replicate the core components
 * of an Actor Oriented Model View Controller (AOMViC).
 * 
 * First task is to get an enum selection working with a while loop.
 * Second task is to add message queueing
 * Third task is to create parallel threads of execution
 * 
 * I will be trying to document this well to create a standard for
 * how we document C++ code moving forward.
 * 
 * Joshua Pratt - 8/27/2021
*/

// used for file input/output which includes the console
#include <iostream> 

// used to make string manipulation easier. instead of working with char arrays
#include <string> 

// used to abbreviate commands instead of specifying "std::" in front of items like "cout" or "cin"
using namespace std; 

// used to queue messages between locations
#include <queue>

// declaring aomvic_msg with all the messages that can be sent
enum aomvic_msg{
    init,
    demo,
    quit,
    error
};

//declaring a cluster for the future use of having a void pointer for data associated with the message
struct aomvic_msg_cluster{
    aomvic_msg msg;
};

// declare prototype since the declaration happens after main
void internal_processing(queue<aomvic_msg_cluster> internalq);

int main(){
    
    // declare memory for queues in main to allow the queues to be passed to other loops
    queue<aomvic_msg_cluster> internalq;
    
    // queue states for demonstration
    internalq.push({init});
    internalq.push({demo});
    internalq.push({error});

    // always make sure to queue a quit since there isn't a timeout like LabVIEW yet
    internalq.push({quit});

    // set up a separate function to make second and third task easier down the road
    internal_processing(internalq); 
    return 0;
}

// declaration of internal processing function with a queue input
void internal_processing(queue<aomvic_msg_cluster> internalq){
    
    // initialize variables for loop condition
    aomvic_msg_cluster internal = {init};

    // starting while loop for dequeueing messages from queue
    while(internal.msg != quit){

        // if internal queue is not empty, then pop an element off of the queue
        if( !internalq.empty() ){

            // get the value from the front of the queue
            internal = internalq.front();

            // remove the front most value from the queue
            internalq.pop();
    
            // declare a case structure on our variable internal.msg
            switch (internal.msg){
                case init:
                    cout << "initialized internal processing" << endl; 

                    // tells switch statement to exit and not run anymore cases for this iteration
                    break; 

                // the next case only executes if internal.msg == error
                case error:

                    // output to terminal that out program has an error
                    cout << "internal processing received an error" << endl;
                    break;

                // demo case is used to move around and test the functionality that we should be implementing in the AOMViC
                case demo:
                    cout << "this is a demonstration of the functionality of AOMViC" << endl;
                    break;

                // execute default if internal.msg is not captured by the cases before
                default:
                    break;
            }
        }
    }
    cout << "we have quit internal processing" << endl;
}